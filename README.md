# Graphic Implementation of Robotic Applications using NodeRED 

**How to try it !**

* [ ] mkdir my_project
* [ ] cd my_project --> Git init and Clone the Repo
* [ ] Just run the server.js file inside the node-js-server/server. 
* [ ] Connect localhost:8080 in the web-server. 
* [ ] You can upload a yaml file that base on the implementation of Swagger Specification describing REST calls and Web Socket calls, also based in the Robotics4All (R4A) architecture. (The repo comes already into the loadig file with 2 yaml files for testing)
* [ ] After upload, automatically is been created a node-red-contrib-{name} package in ../my_project/2017_b_nodered_robotics/node-js-server/server/uploaded_files/js_html_json/ with all nodes serving the calls described in the yaml file!

**In order to link it and use your new nodes in Node-Red:**

1.  `cd my_project/2017_b_nodered_robotics/node-js-server/server/uploaded_files/js_html_json/node-red-contrib-{name}`
2.  `sudo npm link`
3.  `cd ~/.node-red`
4.  `npm link node-red-contrib-{name}`
5.  `cd`
6.  `node-red`




Enjoy !  

**Prerequisite is to install NodeRed, npm and the depedancies required in the server.js file*

