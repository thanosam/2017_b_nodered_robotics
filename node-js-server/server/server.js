//UPLOAD ONLY YAML

var http = require('http');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
const SwaggerParser = require("@apidevtools/swagger-parser");
const pug = require('pug');
var nunjucks = require('nunjucks');
var unirest = require('unirest');
var axios = require('axios');
var querystring = require('querystring');
var FormData = require('form-data');
var request = require('request');
var flag=0;
var directories;
var description;


http.createServer(function (req, res) { // creates server
  if (req.url == '/fileupload') {
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var oldpath = files.filetoupload.path;
      var newpath = __dirname + '/uploaded_files/uploaded_file_' + files.filetoupload.name;

      fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
        res.write('File uploaded and moved! New name : uploaded_file_PREVIOUSFILENAME. js htlm json files have already been created');
        res.end();
	flag++;
///////////////////////////////////////////////////////////
   //if in the folder uploaded_files there is file
   if (flag>0){
     SwaggerParser.dereference(__dirname + "/uploaded_files/uploaded_file_" + files.filetoupload.name)
       .then(function(api) {
         paths = api['paths'];
         var scheme = "http://"
         var url=scheme +  api['host'] + api['basePath'];
         var wsurl= "ws://" + api['wshost'] + api['basePath'];
         console.log(url);
         var title= api['info']['title'];
         title = title.replace(/ /g, "");
         var descr = api['info']['description'];
         var ver = api['info']['version'];
         var forjsondir = [];
         var objectjson;
         var j = 0;

         // creates a node-red-contrib-' folder
         fs.mkdir(__dirname + '/uploaded_files/js_html_json/node-red-contrib-'+title, function(){
          return;
        });
         for (var path in paths){
           for(var method in paths[path]){
             var paramstogive=[];
             var jsdata;
             var htmldata;
             var htmlwsdata;
             var jswsdata;
             var wsdirectories;
             var countpath=0;
             var countquery=0;
             var countbody=0;
             var countheader=0;
             var countformData=0;
             var counterror=0;
             var count=0;
             var bname=[];
             var qname=[];
             var pname=[];
             var fDname=[];
             var hname=[];
             var qvalue=[];
             var bvalue=[];
             var pvalue=[];
             var fDvalue=[];
             var hvalue=[];
             var tempor;
             var params= {};
             var op;
             var data= {};
             var od;
             var opath={};
             var opa;
             var formD={};
             var ofd;
             var dir;
             dir = path;
             var thename;
             var tags;
             var d = path + '_' + method ;
             directories = d.replace(/\//g, "_");
             directories = directories.replace(/{/g, "");
             directories = directories.replace(/}/g, "");
             forjsondir[j] = '"'+directories+'"'+':'+'"'+directories+'.js"';
             j = j + 1 ;
             var xid= [];
             var idcounter = 0;
             var xidforjs= [];
             var checkxid= [];
             var xmerge = false;
             var implemented=true;
             var stream = false;
             for(var attr in paths[path][method]){
               if (attr == 'x-implemented'){
                 if (!paths[path][method][attr]){
                  implemented= false;
                 }
               }
               if (attr == "x-streamable"){
                 if (paths[path][method][attr]){
                   stream = true;
                 }
               }
               if (attr == 'x-ids'){
                 for (var id in paths[path][method][attr]){
                   xid[idcounter]=paths[path][method][attr][idcounter];
                   checkxid[idcounter]="'"+paths[path][method][attr][idcounter]+"'";
                   xidforjs[idcounter]= "'"+xid[idcounter]+"':'value'";
                   xid[idcounter]= '<option value="'+xid[idcounter]+'">'+xid[idcounter]+'</option><br/>'
                   idcounter = idcounter + 1 ;
                 }
               }
               if (attr == 'x-merge'){
                 if (paths[path][method][attr]){
                  xmerge= true;
                 }
               }
               if (attr == 'tags'){
                 tags= paths[path][method][attr][0];
               }
               if(attr == 'description'){
                 description = paths[path][method][attr];
               }//closes the if
               if(attr == 'parameters'){
                 var w=0;
                 for (var p in paths[path][method][attr]){
                   var name = paths[path][method][attr][p]['name'];
                   var inside =  paths[path][method][attr][p]['in'];
                   var type = paths[path][method][attr][p]['type'];
                   paramstogive[w]=name;
                   w=w + 1;
                   if (inside== 'query'){
                     qvalue[countquery]= 'value' + count ;
                     qname[countquery]= name;
                     params[countquery]= "'"+qname[countquery]+"'"+":"+"'value'";
                     count++;
                     countquery++;
                   }else if (inside== 'path') {
                     pvalue[countpath]= 'value' + count;
                     pname[countpath]= name;
                     thename =  "{" + pname[countpath] + "}";
                     tempor= pname[countpath];
                     dir =dir.replace(thename,tempor);
                     opath[countpath]= "'"+pname[countpath]+"'"+":"+"'value'";
                     countpath++;
                     count++;
                   }else if (inside== 'body') {
                     bvalue[countbody]= 'value' + count ;
                     bname[countbody]= name;
                     data[countbody]= "'"+bname[countbody]+"'"+":"+"'value'";
                     count++;
                     countbody++;
                   }else if (inside== 'formData') {
                     fDname[countformData]= name;
                     if (type == 'file'){
                       formD[countformData]= "'"+fDname[countformData]+"'"+":"+"'putpathhere'";
                     }else{
                       formD[countformData]= "'"+fDname[countformData]+"'"+":"+"'value'";
                     }
                     count++;
                     countformData++;
                   }else if (inside== 'header') {
                     hname[countheader]= name;
                     count++;
                     countheader++;
                   }else {
                     count++;
                     counterror++;
                   }
                 }
               }

             }
             if (implemented){
               // creates js
               opa= Object.values(opath);   // object path
               op = Object.values(params); // object params
               od = Object.values(data);
               ofd= Object.values(formD);

               if (countpath>=1){
                 jsdata= nunjucks.render('nunjucksjs.njk', {tags:tags,named:directories,method:method,url:url,urlpath:dir,path:"{"+opa+"}",params:"{"+op+"}",data:"{"+od+"}",formdata:"{"+ofd+"}", xidforjs:"{"+Object.values(xidforjs)+"}"});
               }else{
                 jsdata= nunjucks.render('nunjucksjs.njk', {tags:tags,named:directories,method:method,url:url,urlpath:path,path:"{"+opa+"}",params:"{"+op+"}",data:"{"+od+"}",formdata:"{"+ofd+"}", xidforjs:"{"+Object.values(xidforjs)+"}"});
               }
               fs.appendFile(__dirname + '/uploaded_files/js_html_json/node-red-contrib-'+title+'/'+directories+'.js',
               jsdata,
               function (err) {
                 if (err) throw err;
                 console.log('A .js file created and saved');
               });
               // creates file.html
               htmldata=nunjucks.render('nunjuckshtml.njk',{ name: directories , description: description , tags: tags, paramstogive: paramstogive , xid: Object.values(xid) , hostport: url });
               fs.appendFile(__dirname + '/uploaded_files/js_html_json/node-red-contrib-'+title+'/'+directories+'.html',
               htmldata,
               function (err) {
                 if (err) throw err;
                 console.log('A .html files created and saved');
               });
               if (stream){
                 forjsondir[j] = '"ws'+directories+'"'+':'+'"ws'+directories+'.js"';
                 j = j + 1 ;
                 // creates ws.js
                 wsdirectories= directories.replace("_", "/");
                 jswsdata=nunjucks.render('nunjucksjsws.njk',{ name: 'ws' + directories, wsurl: wsurl , urlpath: wsdirectories , xidforjs:"{"+Object.values(xidforjs)+"}"  });
                 fs.appendFile(__dirname + '/uploaded_files/js_html_json/node-red-contrib-'+title+'/ws'+directories+'.js',
                 jswsdata,
                 function (err) {
                   if (err) throw err;
                   console.log('A .js web socket file created and saved');
                 });
                 // creates ws.html
                 htmlwsdata=nunjucks.render('nunjuckshtmlws.njk',{ name: 'ws' + directories, description: description , xid: Object.values(xid) , hostport: wsurl});
                 fs.appendFile(__dirname + '/uploaded_files/js_html_json/node-red-contrib-'+title+'/ws'+directories+'.html',
                 htmlwsdata,
                 function (err) {
                   if (err) throw err;
                   console.log('A .html web socket file created and saved');
                 });
               }
             }


           }
         }
         //creates file.json
         objectjson= Object.values(forjsondir);
         var l = nunjucks.render('nunjucksjson.njk', { title: 'node-red-contrib-' + title, version: ver, description: descr,  nodes: objectjson })
         fs.appendFile(__dirname + '/uploaded_files/js_html_json/node-red-contrib-'+title+'/package.json',
          l,
          function (err) {
            if (err) throw err;
              console.log('A .json file created and saved');
          });
       });

     }
///////////////////////////////////////////////////////////
      });

   });




  } else {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
    res.write('<input type="file" name="filetoupload"><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end();
  }

}).listen(8080);
console.log("Server Started at port 8080");
console.log("Upload a file");
console.log("* only files .yaml are acceptable");
