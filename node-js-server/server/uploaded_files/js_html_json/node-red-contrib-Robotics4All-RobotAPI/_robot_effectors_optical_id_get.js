module.exports = function(RED) {

var unirest = require('unirest');
var unirest = require('unirest');
var axios = require('axios');
var querystring = require('querystring');
var fs= require('fs');
var path = require('path')
var FormData = require('form-data');
var request = require('request');
var value;

    function EffectorsOptical(config) {
        RED.nodes.createNode(this,config);

        var node = this;
        var param = [];
        var i=0;

        var bodycontext={};

        function isEmpty(obj) {
            for(var key in obj) {
                if(obj.hasOwnProperty(key))
                    return false;
            }
            return true;
        }

        var url;
        if (!config.url) {
          url = 'http://155.207.33.185:8080' + '/robot/effectors/optical/id';
        }else{
          url = config.url + '/robot/effectors/optical/id';
          if (url[0] !== 'h') {
            url = 'https://'+url;
          }

        }

  

        var path = {'id':'value'};
        var query = {};
        var body = {};
        var formdata = {};
        var xidforjs = {'Brain0':'value','Brain1':'value','Brain10':'value','Brain11':'value','Brain2':'value','Brain3':'value','Brain4':'value','Brain5':'value','Brain6':'value','Brain7':'value','Brain8':'value','Brain9':'value','ChestLedsBlue':'value','ChestLedsGreen':'value','ChestLedsRed':'value','LeftEarLed1':'value','LeftEarLed10':'value','LeftEarLed2':'value','LeftEarLed3':'value','LeftEarLed4':'value','LeftEarLed5':'value','LeftEarLed6':'value','LeftEarLed7':'value','LeftEarLed8':'value','LeftEarLed9':'value','LeftFaceLed1':'value','LeftFaceLed2':'value','LeftFaceLed3':'value','LeftFaceLed4':'value','LeftFaceLed5':'value','LeftFaceLed6':'value','LeftFaceLed7':'value','LeftFaceLed8':'value','LeftFootLedsBlue':'value','LeftFootLedsGreen':'value','LeftFootLedsRed':'value','RightEarLed1':'value','RightEarLed10':'value','RightEarLed2':'value','RightEarLed3':'value','RightEarLed4':'value','RightEarLed5':'value','RightEarLed6':'value','RightEarLed7':'value','RightEarLed8':'value','RightEarLed9':'value','RightFaceLed1':'value','RightFaceLed2':'value','RightFaceLed3':'value','RightFaceLed4':'value','RightFaceLed5':'value','RightFaceLed6':'value','RightFaceLed7':'value','RightFaceLed8':'value','RightFootLedsBlue':'value','RightFootLedsGreen':'value','RightFootLedsRed':'value'};


	       // receiving messages
        node.on('input', function(parameters) {


          for (att in parameters){

            if (att != "_msgid"){

            console.log(att);
            console.log(parameters[att]);

        if (isEmpty(xidforjs)){
           for (obj in path){
                 if (obj ==att){
                   //replace path parameters according to the parameters that we give via node-red
                   path[obj]=parameters[att];
                   url = url.replace(obj, path[obj]);
                 }
            }
        }else{
          for (obj in path){
              url = url.replace(obj, config.ids);
         }
        }


            for (obj in query){
              if (obj == att){
                //replace query parameters according to the parameters that we give via node-red
                query[obj]=parameters[att];
                }
            }
            for (obj in body){
              if (obj == att){
                //replace body parameters according to the parameters that we give via node-red
                body[obj]=parameters[att];
                bodycontext=body[obj];
              }
            }
            for (obj in formdata){
              if (obj == att){
                //replace formdata parameters according to the parameters that we give via node-red
                formdata[obj]=parameters[att];
              }
            }

            } // telos apo if(att.....)

          } //telos analushs input

        console.log(url);
        console.log(path);
        console.log(query);
        console.log(body);
        console.log(formdata);






        // AN DEN EXW FORMDATA KANW KANONIKA KLISI AXIOS
        if (isEmpty(formdata)){
          //send request to host server
          axios({
            method:'get',
            url: url ,
            //headers: {'X-Mashape-Key': '5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI'},
            params: query,
            data:  bodycontext
          })
          .then(function(response) {
             console.log(response.data);
             console.log(response.status);
             //console.log(response.statusText);
             //console.log(response.headers);
             //console.log(response.config);

             //receiving response and send it to node-red
             //node.send(response.headers);
             node.send(response.data);
             //node.send(response.status);
          })
          .catch(function (error) {
              console.log(error);
              node.send(error);
          });
       // AN EXW FORMDATA ELEGXW AN EXW QUERY .. AN DEN EXW QUERY STELNW KANONIKA URL KAI FORMDATA
        }else{
          if (isEmpty(query)){
            request.get({url:url , formData: formdata}, function(err, httpResponse, resbody) {
              if (err) {
                node.send(error);
                return console.error('upload failed:', err);
              }
              console.log('Upload successful!  Server responded with:', resbody);
              console.log(httpResponse.statusCode);
              //node.send(httpResponse.headers);
              node.send(resbody);
              //node.send(httpResponse.statusCode);
            });
          // ALLIWS AN EXW FORMDATA KAI EXW KAI QUERY ALLAZW TO URL KAI TOU VAZW KAI TA QUERY TA PARAMETERS
          }else{
            url = url + "?";
            for (p in query){
              url = url + p + "=" + query[p] + "&";
            }
            url = url.slice(0,-1);
            request.get({url:url , formData: formdata}, function(err, httpResponse, resbody) {
              if (err) {
                node.send(error);
                return console.error('upload failed:', err);
              }
              console.log('Upload successful!  Server responded with:', resbody);
              console.log(httpResponse.statusCode);
              //node.send(httpResponse.headers);
              node.send(resbody);
              //node.send(httpResponse.statusCode);
            });
          }// TELOS IF TOU IS EMPTY( QUERY )

        }// TELOS IF TOU IS EMPTY( FORMDATA)


      });
	this.status({fill:"red",shape:"ring",text:"disconnected"});
	this.status({fill:"green",shape:"dot",text:"connected"});

  }
    RED.nodes.registerType("_robot_effectors_optical_id_get",EffectorsOptical);
};
module.exports = function(RED) {

var unirest = require('unirest');
var unirest = require('unirest');
var axios = require('axios');
var querystring = require('querystring');
var fs= require('fs');
var path = require('path')
var FormData = require('form-data');
var request = require('request');
var value;

    function EffectorsOptical(config) {
        RED.nodes.createNode(this,config);

        var node = this;
        var param = [];
        var i=0;

        var bodycontext={};

        function isEmpty(obj) {
            for(var key in obj) {
                if(obj.hasOwnProperty(key))
                    return false;
            }
            return true;
        }

        var url;
        if (!config.url) {
          url = 'http://155.207.33.185:8080' + '/robot/effectors/optical/id';
        }else{
          url = config.url + '/robot/effectors/optical/id';
          if (url[0] !== 'h') {
            url = 'https://'+url;
          }

        }

  

        var path = {'id':'value'};
        var query = {};
        var body = {};
        var formdata = {};
        var xidforjs = {'Brain0':'value','Brain1':'value','Brain10':'value','Brain11':'value','Brain2':'value','Brain3':'value','Brain4':'value','Brain5':'value','Brain6':'value','Brain7':'value','Brain8':'value','Brain9':'value','ChestLedsBlue':'value','ChestLedsGreen':'value','ChestLedsRed':'value','LeftEarLed1':'value','LeftEarLed10':'value','LeftEarLed2':'value','LeftEarLed3':'value','LeftEarLed4':'value','LeftEarLed5':'value','LeftEarLed6':'value','LeftEarLed7':'value','LeftEarLed8':'value','LeftEarLed9':'value','LeftFaceLed1':'value','LeftFaceLed2':'value','LeftFaceLed3':'value','LeftFaceLed4':'value','LeftFaceLed5':'value','LeftFaceLed6':'value','LeftFaceLed7':'value','LeftFaceLed8':'value','LeftFootLedsBlue':'value','LeftFootLedsGreen':'value','LeftFootLedsRed':'value','RightEarLed1':'value','RightEarLed10':'value','RightEarLed2':'value','RightEarLed3':'value','RightEarLed4':'value','RightEarLed5':'value','RightEarLed6':'value','RightEarLed7':'value','RightEarLed8':'value','RightEarLed9':'value','RightFaceLed1':'value','RightFaceLed2':'value','RightFaceLed3':'value','RightFaceLed4':'value','RightFaceLed5':'value','RightFaceLed6':'value','RightFaceLed7':'value','RightFaceLed8':'value','RightFootLedsBlue':'value','RightFootLedsGreen':'value','RightFootLedsRed':'value'};


	       // receiving messages
        node.on('input', function(parameters) {


          for (att in parameters){

            if (att != "_msgid"){

            console.log(att);
            console.log(parameters[att]);

        if (isEmpty(xidforjs)){
           for (obj in path){
                 if (obj ==att){
                   //replace path parameters according to the parameters that we give via node-red
                   path[obj]=parameters[att];
                   url = url.replace(obj, path[obj]);
                 }
            }
        }else{
          for (obj in path){
              url = url.replace(obj, config.ids);
         }
        }


            for (obj in query){
              if (obj == att){
                //replace query parameters according to the parameters that we give via node-red
                query[obj]=parameters[att];
                }
            }
            for (obj in body){
              if (obj == att){
                //replace body parameters according to the parameters that we give via node-red
                body[obj]=parameters[att];
                bodycontext=body[obj];
              }
            }
            for (obj in formdata){
              if (obj == att){
                //replace formdata parameters according to the parameters that we give via node-red
                formdata[obj]=parameters[att];
              }
            }

            } // telos apo if(att.....)

          } //telos analushs input

        console.log(url);
        console.log(path);
        console.log(query);
        console.log(body);
        console.log(formdata);






        // AN DEN EXW FORMDATA KANW KANONIKA KLISI AXIOS
        if (isEmpty(formdata)){
          //send request to host server
          axios({
            method:'get',
            url: url ,
            //headers: {'X-Mashape-Key': '5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI'},
            params: query,
            data:  bodycontext
          })
          .then(function(response) {
             console.log(response.data);
             console.log(response.status);
             //console.log(response.statusText);
             //console.log(response.headers);
             //console.log(response.config);

             //receiving response and send it to node-red
             //node.send(response.headers);
             node.send(response.data);
             //node.send(response.status);
          })
          .catch(function (error) {
              console.log(error);
              node.send(error);
          });
       // AN EXW FORMDATA ELEGXW AN EXW QUERY .. AN DEN EXW QUERY STELNW KANONIKA URL KAI FORMDATA
        }else{
          if (isEmpty(query)){
            request.get({url:url , formData: formdata}, function(err, httpResponse, resbody) {
              if (err) {
                node.send(error);
                return console.error('upload failed:', err);
              }
              console.log('Upload successful!  Server responded with:', resbody);
              console.log(httpResponse.statusCode);
              //node.send(httpResponse.headers);
              node.send(resbody);
              //node.send(httpResponse.statusCode);
            });
          // ALLIWS AN EXW FORMDATA KAI EXW KAI QUERY ALLAZW TO URL KAI TOU VAZW KAI TA QUERY TA PARAMETERS
          }else{
            url = url + "?";
            for (p in query){
              url = url + p + "=" + query[p] + "&";
            }
            url = url.slice(0,-1);
            request.get({url:url , formData: formdata}, function(err, httpResponse, resbody) {
              if (err) {
                node.send(error);
                return console.error('upload failed:', err);
              }
              console.log('Upload successful!  Server responded with:', resbody);
              console.log(httpResponse.statusCode);
              //node.send(httpResponse.headers);
              node.send(resbody);
              //node.send(httpResponse.statusCode);
            });
          }// TELOS IF TOU IS EMPTY( QUERY )

        }// TELOS IF TOU IS EMPTY( FORMDATA)


      });
	this.status({fill:"red",shape:"ring",text:"disconnected"});
	this.status({fill:"green",shape:"dot",text:"connected"});

  }
    RED.nodes.registerType("_robot_effectors_optical_id_get",EffectorsOptical);
};
