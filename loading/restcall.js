var unirest = require('unirest');
var axios = require('axios');
var querystring = require('querystring');
var fs= require('fs');
var path = require('path')
var FormData = require('form-data');
var request = require('request');

//
// // These code snippets use an open-source library. http://unirest.io/nodejs
// unirest.get("https://simple-weather.p.mashape.com/weather?lat=40.120875&lng=9.012893")
// .header("X-Mashape-Key", "5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI")
// //.header("Accept", "text/plain")
// .end(function (result) {
//   console.log(result.status , result.headers, result.body);
// });



// axios({
//   method:'get',
//   url:'https://simple-weather.p.mashape.com/weather',
//   params: {
//     lat: 40.120875,
//     lng: 9.012893
//   },
//   headers: {'X-Mashape-Key': '5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI'},
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    //console.log(response.config);
// });

// // These code snippets use an open-source library. http://unirest.io/nodejs
// unirest.post("https://neutrinoapi-phone-validate.p.mashape.com/phone-validate")
// .header("X-Mashape-Key", "5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI")
// //.header("Content-Type", "application/x-www-form-urlencoded")
// //.header("Accept", "application/json")
// //.send("country-code=GB")
// .send("number=+447522123456")
// .end(function (result) {
//   console.log(result.status, result.headers, result.body);
// });


// axios({
//   method:'post',
//   url:'https://neutrinoapi-phone-validate.p.mashape.com/phone-validate',
//   headers: {'X-Mashape-Key': '5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI'},
//   data: querystring.stringify({
//     'country-code': 'GR',
//     'number': '+306922123456'
//   })
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    //console.log(response.config);
// })
// .catch(function (error) {
//     console.log(error);
// });







// // These code snippets use an open-source library. http://unirest.io/nodejs
// unirest.get("https://numbersapi.p.mashape.com/6/21/date?fragment=true&json=true")
// .header("X-Mashape-Key", "5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI")
// .header("Accept", "text/plain")
// .end(function (result) {
//   console.log(result.status, result.headers, result.body);
// });


// var a=1;
// var ab=2;
// axios({
//   method:'get',
//   url:'https://numbersapi.p.mashape.com/'+a+'/'+ab+'/date',
//   params: {
//     'fragment': 'true',
//     'json': 'true'
//   },
//   headers: {'X-Mashape-Key': '5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI'},
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    //console.log(response.config);
// });


// // These code snippets use an open-source library. http://unirest.io/nodejs
// unirest.post("https://upload.p.mashape.com/base/")-
// .header("X-Mashape-Key", "5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI")
// .attach("file", fs.createReadStream("ΠΡΟΓΡΑΜΜΑ ΕΞΕΤΑΣΕΩΝ ΣΕΠΤΕΜΒΡΙΟΥ 2018.pdf"))
// .field("UPLOADCARE_PUB_KEY", "demopublickey")
// //.field("UPLOADCARE_STORE", 1)
// .end(function (result) {
//   console.log(result.status, result.headers, result.body);
// });


// var form = new FormData();
// form.append('UPLOADCARE_PUB_KEY', 'demopublickey');
// //data.append('my_buffer', new Buffer(10));
// form.append('file', fs.createReadStream('ΠΡΟΓΡΑΜΜΑ ΕΞΕΤΑΣΕΩΝ ΣΕΠΤΕΜΒΡΙΟΥ 2018.pdf'));
// axios({
//   method:'post',
//   url:'https://upload.p.mashape.com/base/',
//   headers: {'X-Mashape-Key': '5Nqemga4xJmshlbF9aCt3IMUzkBcp1vkJvgjsnPVol4c9tBXgI'},
//   transformRequest: [function (form, headers) {
//     var form = new FormData();
//     form.append('UPLOADCARE_PUB_KEY', 'demopublickey');
//     //data.append('my_buffer', new Buffer(10));
//     form.append('file', fs.createReadStream('ΠΡΟΓΡΑΜΜΑ ΕΞΕΤΑΣΕΩΝ ΣΕΠΤΕΜΒΡΙΟΥ 2018.pdf'));
//
//     return form;
//   }],
//   data: form
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    //console.log(response.config);
// })
// .catch(function (error) {
//     console.log(error);
// });


////////////////////////////////////////////////petstore////////////////////////////////////////////////////

// axios({
//   method:'get',
//   url:'https://petstore.swagger.io/v2/pet/findByStatus',
//   params: {
//     'status': 'available'
//   },
//   //headers: {'api_key': 'special-key'},
// })
// .then(function(response) {
//    //console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    console.log(response.headers);
//    //console.log(response.config);
// });
//
//
//
// axios({
//   method:'post',
//   url:'https://petstore.swagger.io/v2/pet',
//   data: {id:'9'}
//
//  //headers: {'api_key': 'special-key'},
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    //console.log(response.config);
// });
//
//
// axios({
//   method:'put',
//   url:'https://petstore.swagger.io/v2/pet',
//   data: {
//   "id": 9,
//   "category": {
//     "id": 0,
//     "name": "string"
//   },
//   "name": "skili",
//   "photoUrls": [
//     "string"
//   ],
//   "tags": [
//     {
//       "id": 0,
//       "name": "string"
//     }
//   ],
//   "status": "available"
// },
//  //headers: {'api_key': 'special-key'},
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    //console.log(response.config);
// });
//
//
//
//
//
//
//
//path
// axios({
//   method:'get',
//   url:'https://petstore.swagger.io/v2/pet/2',
//  //headers: {'api_key': 'special-key'},
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    //console.log(response.config);
// });

//form data
// axios({
//   method:'post',
//   url:'https://petstore.swagger.io/v2/pet/1',
//   data: querystring.stringify({
//      'name': 'arouraios',
//      'status': 'available'
//    }),
//  //headers: {'accept': 'application/x-www-form-urlencoded'},
// })
// .then(function(response) {
//    //console.log(response.data);
//    //console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    console.log(response.config);
// });

// var formData = {
//   additionalMetadata: 'maa',
//   file: fs.createReadStream('/home/thanosam/Desktop/ΠΡΟΓΡΑΜΜΑ ΕΞΕΤΑΣΕΩΝ ΣΕΠΤΕΜΒΡΙΟΥ 2018.pdf')
// };
//
// request.post({url:'https://petstore.swagger.io/v2/pet/3/uploadImage', formData: formData}, function(err, httpResponse, body) {
//   if (err) {
//     return console.error('upload failed:', err);
//   }
//   console.log('Upload successful!  Server responded with:', body);
//   console.log(httpResponse.statusCode);
// });


// function isEmpty(obj) {
//     for(var key in obj) {
//         if(obj.hasOwnProperty(key))
//             return false;
//     }
//     return true;
// }
//
// var url= 'www.kati/katial/asfs/'
// var query = {'username': "thanos", "password": "039", 'asfafsaas':"asfafsafsaf", "afasfsad": "s", "asfsafasd": 'asfafsafs'};
// if (isEmpty(query)){
//   console.log(url);
// }else{
//   url = url + "?"
//   for (p in query){
//     url  = url + p + "=" + query[p] + "&";
//   }
//   url = url.slice(0,-1);
//   console.log(url);
// }

// var formData = new FormData();
// formData.append('additionalMetadata', 'asd');
// formData.append('file',fs.createReadStream(path.join(__dirname, 'backendfrontendexample.jpg')));
// var d = fs.createReadStream('ΠΡΟΓΡΑΜΜΑ ΕΞΕΤΑΣΕΩΝ ΣΕΠΤΕΜΒΡΙΟΥ 2018.pdf');
// axios({
//   method:'post',
//   url:'https://petstore.swagger.io/v2/pet/3/uploadImage',
//   data: formData,
//   config: { headers: {'Content-Type': 'multipart/form-data'} }
//   //headers: {'Content-Type': 'multipart/form-data'}
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    console.log(response.config);
// })
// .catch(function (error) {
//      console.log(error);
// });


// axios({
//   method:'delete',
//   url:'https://petstore.swagger.io/v2/pet/12',
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    console.log(response.config);
// })
// .catch(function (error) {
//       console.log(error);
//  });

// var d = fs.createReadStream('ΠΡΟΓΡΑΜΜΑ ΕΞΕΤΑΣΕΩΝ ΣΕΠΤΕΜΒΡΙΟΥ 2018.pdf');
//
// var formData = new FormData();
// formData = { additionalMetadata : "asdfghjk",
// file : d}
// //formData.append('additionalMetadata', 'asd');
// //formData.append('file',fs.createReadStream(path.join(__dirname, 'backendfrontendexample.jpg')));
// var formData= new FormData();
//  formData = {
//   additionalMetadata: 'maa',
//   file: fs.createReadStream(__dirname + '/ΠΡΟΓΡΑΜΜΑ ΕΞΕΤΑΣΕΩΝ ΣΕΠΤΕΜΒΡΙΟΥ 2018.pdf')
// };
//
// axios({
//   method:'post',
//   url:'https://petstore.swagger.io/v2/pet/0/uploadImage',
//   data: formData,
//   //config:  { headers: {'Content-Type': 'multipart/form-data'} }
//   //headers: {'Content-Type': 'multipart/form-data'}
// })
// .then(function(response) {
//    console.log(response.data);
//    console.log(response.status);
//    //console.log(response.statusText);
//    //console.log(response.headers);
//    console.log(response.config);
// })
// .catch(function (error) {
//      console.log(error);
// });
